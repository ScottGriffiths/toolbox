namespace Aperture.Core.Tests.Types.PipelineTests.Extensions.PipelineResolver
{
    using _Helpers;
    using Core.Types;
    using FluentAssertions;
    using NUnit.Framework;

    [TestFixture]
    public class WhenSuccessful 
    {
        private string _response;

        [SetUp]
        public void SetUp()
        {
            _response = Pipeline.Start("Response")
                .Resolve(TestPipelineHandler.Resolve);
        }
        
        [Test] public void ThenTheCorrectResponseIsReturned() => _response.Should().Be("Response");
        
    }
}