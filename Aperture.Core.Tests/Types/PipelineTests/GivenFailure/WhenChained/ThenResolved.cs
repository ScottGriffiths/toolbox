namespace Aperture.Core.Tests.Types.PipelineTests.GivenFailure.WhenChained
{
    using _Helpers;
    using Core.Types;
    using FluentAssertions;
    using NUnit.Framework;

    [TestFixture]
    public class ThenResolved
    {
        private string _response;
        private readonly MockPipelineFunctions _mock = new MockPipelineFunctions();

        [SetUp]
        public void SetUp()
        {
            _response = Pipeline.Cancel<string>(Failure.Because("Error"))
                .Then(_mock.Then)
                .Resolve(TestPipelineHandler.Handle);
        }

        [Test] public void ThenTheCorrectResponseIsReturned() => _response.Should().Be("Failure - Message: 'Error'");

        [Test] public void ThenTheThenIsNotCalled() => _mock.ThenCalled.Should().BeFalse();
    }
}