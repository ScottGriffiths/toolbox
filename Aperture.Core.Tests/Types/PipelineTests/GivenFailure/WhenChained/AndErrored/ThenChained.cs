namespace Aperture.Core.Tests.Types.PipelineTests.GivenFailure.WhenChained.AndErrored
{
    using _Helpers;
    using Core.Types;
    using FluentAssertions;
    using NUnit.Framework;

    [TestFixture]
    public class ThenChained
    {
        private string _response;
        private readonly MockPipelineFunctions _mock = new MockPipelineFunctions();

        [SetUp]
        public void SetUp()
        {
            _response = Pipeline.Cancel<string>(Failure.Because("Another Error"))
                .Then(_mock.ThenFail)
                .Then(_mock.Then)
                .Resolve(TestPipelineHandler.Handle);
        }

        [Test] public void ThenTheCorrectResponseIsReturned() => _response.Should().Be("Failure - Message: 'Another Error'");

        [Test] public void ThenTheMapIsNotCalled() => _mock.ThenCalled.Should().BeFalse();
        
        [Test] public void ThenTheThenFailIsNotCalled() => _mock.ThenFailedCalled.Should().BeFalse();
    }
}