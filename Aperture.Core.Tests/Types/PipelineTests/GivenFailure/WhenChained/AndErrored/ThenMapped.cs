namespace Aperture.Core.Tests.Types.PipelineTests.GivenFailure.WhenChained.AndErrored
{
    using _Helpers;
    using Core.Types;
    using FluentAssertions;
    using NUnit.Framework;

    [TestFixture]
    public class ThenMapped
    {
        private string _response;
        private readonly MockPipelineFunctions _mock = new MockPipelineFunctions();

        [SetUp]
        public void SetUp()
        {
            _response = Pipeline.Cancel<string>(Failure.Because("Another Error"))
                .Then(_mock.ThenFail)
                .Map(_mock.Map)
                .Resolve(TestPipelineHandler.Handle);
        }

        [Test] public void ThenTheCorrectResponseIsReturned() => _response.Should().Be("Failure - Message: 'Another Error'");

        [Test] public void ThenTheMapIsNotCalled() => _mock.MapCalled.Should().BeFalse();
        
        [Test] public void ThenTheThenFailIsNotCalled() => _mock.ThenFailedCalled.Should().BeFalse();
    }
}