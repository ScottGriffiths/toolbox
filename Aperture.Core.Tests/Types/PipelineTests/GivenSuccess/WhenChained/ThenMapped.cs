namespace Aperture.Core.Tests.Types.PipelineTests.GivenSuccess.WhenChained
{
    using _Helpers;
    using Core.Types;
    using FluentAssertions;
    using NUnit.Framework;

    [TestFixture]
    public class ThenMapped
    {
        private string _response;
        private readonly MockPipelineFunctions _mock = new MockPipelineFunctions();

        [SetUp]
        public void SetUp()
        {
            _response = Pipeline.Start("Success")
                .Then(_mock.Then)
                .Map(_mock.Map)
                .Resolve(TestPipelineHandler.Handle);
        }

        [Test]
        public void ThenTheCorrectResponseIsReturned() => _response.Should()
            .Be("Success - Chained - Mapped");
    }
}