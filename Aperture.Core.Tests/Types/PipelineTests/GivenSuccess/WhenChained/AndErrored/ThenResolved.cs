namespace Aperture.Core.Tests.Types.PipelineTests.GivenSuccess.WhenChained.AndErrored
{
    using _Helpers;
    using Core.Types;
    using FluentAssertions;
    using NUnit.Framework;

    [TestFixture]
    public class ThenResolved
    {
        private string _response;
        private readonly MockPipelineFunctions _mock = new MockPipelineFunctions();

        [SetUp]
        public void SetUp()
        {
            _response = Pipeline.Start("Success")
                .Then(_mock.ThenFail)
                .Resolve(TestPipelineHandler.Handle);
        }

        [Test]
        public void ThenTheCorrectResponseIsReturned() => _response.Should()
            .Be("Failure - Message: 'Error'");
    }
}