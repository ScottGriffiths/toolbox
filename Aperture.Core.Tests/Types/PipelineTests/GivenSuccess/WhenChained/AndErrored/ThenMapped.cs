namespace Aperture.Core.Tests.Types.PipelineTests.GivenSuccess.WhenChained.AndErrored
{
    using System;
    using _Helpers;
    using Core.Types;
    using FluentAssertions;
    using NUnit.Framework;

    [TestFixture]
    public class ThenMapped
    {
        private string _response;
        private readonly MockPipelineFunctions _mock = new MockPipelineFunctions();

        [SetUp]
        public void SetUp()
        {
            _response = Pipeline.Start("Success")
                .Then(_mock.ThenFail)
                .Map(_mock.Map)
                .Resolve(TestPipelineHandler.Handle);
        }

        [Test]
        public void ThenTheCorrectResponseIsReturned() => _response.Should()
            .Be("Failure - Message: 'Error'");

        [Test]
        public void ThenTheMapIsNotCalled() => _mock.MapCalled.Should()
            .BeFalse();
    }
}