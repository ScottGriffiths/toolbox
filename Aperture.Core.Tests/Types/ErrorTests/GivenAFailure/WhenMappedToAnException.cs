namespace Aperture.Core.Tests.Types.ErrorTests.GivenAFailure
{
    using System;
    using Core.Types;
    using FluentAssertions;
    using NUnit.Framework;

    [TestFixture]
    public class WhenMappedToAnException
    {
        private int _code;
        private string _message;
        private Exception _response;

        [SetUp]
        public void SetUp()
        {
            _code = 101;
            _message = "An error occured.";
            _response = Error.From(Failure.Because(_code, _message))
                .Map(failure => new Exception(failure.Message))
                .Resolve(failure => null, exception => exception);
        }

        [Test]public void ThenTheFailureMessageIsCorrect() => _response.Message.Should().Be("An error occured.");
    }
}