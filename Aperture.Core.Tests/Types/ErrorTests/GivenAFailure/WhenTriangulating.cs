namespace Aperture.Core.Tests.Types.ErrorTests.GivenAFailure
{
    using System;
    using Core.Types;
    using FluentAssertions;
    using NUnit.Framework;

    [TestFixture]
    public class WhenTriangulating
    {
        private string _message;
        private string _response;

        [SetUp]
        public void SetUp()
        {
            _message = "An error occured.";
            _response = Error.From(Failure.Because(_message))
                .Map(failure => Failure.Because(failure.Message + "5")) // Runs
                .Map(failure => new Exception(failure.Message + "6")) // Runs
                .Map(failure => Failure.Because(failure.Message + "7")) // Skipped
                .Map(failure => new Exception(failure.Message + "8")) // Skipped
                .MapException(exception => new Exception(exception.Message + "1")) // Runs
                .MapException(exception => Failure.Because(exception.Message + "2")) // Runs
                .MapException(exception => new Exception(exception.Message + "3")) // Skipped
                .MapException(exception => Failure.Because(exception.Message + "4")) // Skipped
                .Resolve(failure => failure.Message, exception => exception.Message);
        }

        [Test]public void ThenTheFailureMessageIsCorrect() => _response.Should().Be("An error occured.5612");
    }
}