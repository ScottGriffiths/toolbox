namespace Aperture.Core.Tests.Types.ErrorTests.GivenAFailure
{
    using Core.Types;
    using FluentAssertions;
    using NUnit.Framework;

    [TestFixture]
    public class WhenResolved
    {
        private int _code;
        private string _message;
        private Failure _response;

        [SetUp]
        public void SetUp()
        {
            _code = 101;
            _message = "An error occured.";
            _response = Error.From(Failure.Because(_code, _message))
                .Resolve(failure => failure, exception => Failure.Because(500, exception.Message));
        }

        [Test]public void ThenTheFailureCodeIsCorrect() => _response.Code.Should().Be(_code);

        [Test]public void ThenTheFailureMessageIsCorrect() => _response.Message.Should().Be(_message);
    }
}