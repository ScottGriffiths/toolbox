namespace Aperture.Core.Tests.Types.ErrorTests.GivenAFailure
{
    using Core.Types;
    using FluentAssertions;
    using NUnit.Framework;

    [TestFixture]
    public class WhenToStringIsCalled
    {
        private int _code;
        private string _message;
        private Error _response;

        [SetUp]
        public void SetUp()
        {
            _code = 101;
            _message = "An error occured.";
            _response = Error.From(Failure.Because(_code, _message));
        }

        [Test]
        public void ThenTheCorrectMessageIsReturned() => _response.ToString()
            .Should()
            .Be("Failure - Code: '101' Message: 'An error occured.'");
    }
}