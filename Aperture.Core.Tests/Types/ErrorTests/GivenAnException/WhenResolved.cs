namespace Aperture.Core.Tests.Types.ErrorTests.GivenAnException
{
    using System;
    using Core.Types;
    using FluentAssertions;
    using NUnit.Framework;

    [TestFixture]
    public class WhenResolved
    {
        private string _message;
        private Exception _response;

        [SetUp]
        public void SetUp()
        {
            _message = "An error occured.";
            _response = Error.From(new Exception(_message))
                .Resolve(failure =>  null, exception => exception);
        }

        [Test]public void ThenTheFailureMessageIsCorrect() => _response.Message.Should().Be(_message);
    }
}