namespace Aperture.Core.Tests.Types.ErrorTests.GivenAnException
{
    using System;
    using Core.Types;
    using FluentAssertions;
    using NUnit.Framework;

    [TestFixture]
    public class WhenMappedToAnotherException
    {
        private string _message;
        private Exception _response;
        private string _expectedMessage;

        [SetUp]
        public void SetUp()
        {
            _message = "An error occured.";
            _response = Error.From(new Exception(_message))
                .MapException(exception =>
                {
                    _expectedMessage = $"Another Exception from: {exception.Message}";
                    return new Exception(_expectedMessage);
                })
                .Resolve(failure =>  null, exception => exception);
        }

        [Test]public void ThenTheFailureMessageIsCorrect() => _response.Message.Should().Be(_expectedMessage);
    }
}