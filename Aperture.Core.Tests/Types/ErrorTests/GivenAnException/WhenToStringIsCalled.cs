namespace Aperture.Core.Tests.Types.ErrorTests.GivenAnException
{
    using System;
    using Core.Types;
    using FluentAssertions;
    using NUnit.Framework;

    [TestFixture]
    public class WhenToStringIsCalled
    {
        private string _message;
        private Error _response;
        private Exception _exception;

        [SetUp]
        public void SetUp()
        {
            _message = "An error occured.";
            _exception = new Exception(_message);
            _response = Error.From(_exception);
        }

        [Test]
        public void ThenTheCorrectMessageIsReturned() => _response.ToString()
            .Should()
            .Be($"Exception - {_exception}");
    }
}