namespace Aperture.Core.Tests.Types.ErrorTests.GivenAnException
{
    using System;
    using Core.Types;
    using FluentAssertions;
    using NUnit.Framework;

    [TestFixture]
    public class WhenMappedToAFailure
    {
        private string _message;
        private Failure _response;

        [SetUp]
        public void SetUp()
        {
            _message = "An error occured.";
            _response = Error.From(new Exception(_message))
                .MapException(exception => Failure.Because(exception.Message))
                .Resolve(failure =>  failure, exception => Failure.Because(""));
        }

        [Test]public void ThenTheFailureMessageIsCorrect() => _response.Message.Should().Be(_message);
    }
}