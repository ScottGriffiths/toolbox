using NUnit.Framework;

namespace Aperture.Core.Tests.Types.FailureTests
{
    using Core.Types;
    using FluentAssertions;

    [TestFixture]
    public class GivenAFailureMessageAndCode
    {
        private Failure _response;
        private const int ErrorCode = 100;
        private const string ErrorMessage = "Something went wrong";

        [SetUp]
        public void SetUp()
        {
            _response = Failure.Because(ErrorCode, ErrorMessage);
        }

        [Test] public void ThenTheErrorCodeIsMapped() => _response.Code.Should().Be(ErrorCode);
        [Test] public void ThenTheErrorMessageIsMapped() => _response.Message.Should().Be(ErrorMessage);
        [Test] public void ThenToStringReturnsTheCorrectMessage() => _response.ToString().Should().Be("Code: '100' Message: 'Something went wrong'");
    }
}