using NUnit.Framework;

namespace Aperture.Core.Tests.Types.FailureTests
{
    using Core.Types;
    using FluentAssertions;

    [TestFixture]
    public class GivenAFailureMessage
    {
        private Failure _response;
        private const string ErrorMessage = "Something went wrong";

        [SetUp]
        public void SetUp()
        {
            _response = Failure.Because(ErrorMessage);
        }

        [Test] public void ThenTheErrorCodeIsMapped() => _response.Code.Should().Be(0);
        [Test] public void ThenTheErrorMessageIsMapped() => _response.Message.Should().Be(ErrorMessage);
        [Test] public void ThenToStringReturnsTheCorrectMessage() => _response.ToString().Should().Be("Message: 'Something went wrong'");
    }
}