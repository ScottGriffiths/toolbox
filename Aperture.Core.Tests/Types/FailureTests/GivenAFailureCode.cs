using NUnit.Framework;

namespace Aperture.Core.Tests.Types.FailureTests
{
    using Core.Types;
    using FluentAssertions;

    [TestFixture]
    public class GivenAFailureCode
    {
        private Failure _response;
        private const int ErrorCode = 100;

        [SetUp]
        public void SetUp()
        {
            _response = Failure.Because(ErrorCode);
        }

        [Test] public void ThenTheErrorCodeIsMapped() => _response.Code.Should().Be(ErrorCode);
        [Test] public void ThenTheErrorMessageIsMapped() => _response.Message.Should().Be(string.Empty);
        [Test] public void ThenToStringReturnsTheCorrectMessage() => _response.ToString().Should().Be("Code: '100'");
    }
}