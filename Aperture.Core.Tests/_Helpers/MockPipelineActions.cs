namespace Aperture.Core.Tests._Helpers
{
    using System;
    using Core.Types;

    public class MockPipelineFunctions
    {
        private readonly MockFunction<string, string> _mockMap;
        private readonly MockFunction<string, Pipeline<string>> _mockThen;
        private readonly MockFunction<string, Pipeline<string>> _mockThenFailed;

        public MockPipelineFunctions()
        {
            _mockMap = new MockFunction<string, string>(content => $"{content} - Mapped");
            _mockThen = new MockFunction<string, Pipeline<string>>(content => Pipeline.Continue($"{content} - Chained"));
            _mockThenFailed = new MockFunction<string, Pipeline<string>>(content => Failure.Because("Error"));
        }

        public bool MapCalled => _mockMap.Called;
        public bool ThenCalled => _mockThen.Called;
        public bool ThenFailedCalled => _mockThenFailed.Called;

        public string Map(string input) => _mockMap.Call(input);
        public Pipeline<string> Then(string input) => _mockThen.Call(input);
        public Pipeline<string> ThenFail(string input) => _mockThenFailed.Call(input);

        private class MockFunction<TInput, TOutput>
        {
            public bool Called { get; private set; }

            private readonly Func<TInput, TOutput> _func;

            public MockFunction(Func<TInput, TOutput> func)
            {
                _func = func;
            }

            public TOutput Call(TInput input)
            {
                Called = true;
                return _func(input);
            }
        }
    }
}