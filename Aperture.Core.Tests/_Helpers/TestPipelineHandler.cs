namespace Aperture.Core.Tests._Helpers
{
    using Contracts;
    using Core.Types;

    public static class TestPipelineHandler
    {
        public static IPipelineErrorHandler<string> Handle => new TestErrorHandler();
        public static IPipelineResolver<string, string> Resolve => new TestPipelineResolver();
        
        private class TestErrorHandler : IPipelineErrorHandler<string>
        {
            public string Error(Error error) => error.ToString();
        }
        
        private class TestPipelineResolver : IPipelineResolver<string, string>
        {
            public string Error(Error error) => error.ToString();

            public string Success(string input) => input;
        }
    }
}