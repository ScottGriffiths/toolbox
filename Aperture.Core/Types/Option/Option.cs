namespace Aperture.Core.Types.Option
{
    using System;
    using None;

    public struct Option<TValue>
    {
        private bool HasValue { get; }
        private TValue Value { get; }

        private Option(TValue value)
        {
            HasValue = true;
            Value = value;
        }

        private Option(None _)
        {
            HasValue = false;
            Value = default(TValue);
        }
        
        public TResponse Resolve<TResponse>(Func<TValue, TResponse> some, Func<TResponse> none)
            => HasValue? some(Value) : none();

        public static bool IsNull(TValue value) => value == null;
        public static bool IsNotNull(TValue value) => IsNull(value) == false;

        public static implicit operator Option<TValue>(TValue value) => IsNull(value) ? None() : new Option<TValue>(value);
        public static implicit operator Option<TValue>(None none) => new Option<TValue>(none);

        public static Option<TValue> From(TValue value) => value;
        public static Option<TValue> None() => Option.None();
    }
}