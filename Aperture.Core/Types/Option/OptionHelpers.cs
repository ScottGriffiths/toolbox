namespace Aperture.Core.Types.Option
{
    using None;

    public static class Option
    {
        public static Option<TValue> From<TValue>(TValue value) => value;
        public static Option<TValue> None<TValue>() => None();
        public static None None() => new None();

        public static bool IsNull<TValue>(TValue value) => Option<TValue>.IsNull(value);
        public static bool IsNotNull<TValue>(TValue value) => Option<TValue>.IsNotNull(value);
    }
}