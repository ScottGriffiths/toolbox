namespace Aperture.Core.Types
{
    using System;

    public struct Error
    {
        private bool Exceptional { get; }
        private Failure Failure { get; }
        private Exception Exception { get; }

        private Error(Exception exception)
        {
            Exceptional = true;
            Failure = default(Failure);
            Exception = exception;
        }

        private Error(Failure failure)
        {
            Exceptional = false;
            Failure = failure;
            Exception = default(Exception);
        }

        public TResponse Resolve<TResponse>(Func<Failure, TResponse> failure, Func<Exception, TResponse> exception)
            => Exceptional ? exception(Exception) : failure(Failure);

        public static Error From(Failure failure) => failure;
        public static Error From(Exception exception) => exception;

        public static implicit operator Error(Failure failure) => new Error(failure);
        public static implicit operator Error(Exception exception) => new Error(exception);

        public override string ToString() => Exceptional ? 
            $"Exception - {Exception}" : 
            $"Failure - {Failure}";
    }

    public static class ErrorExtensions
    {
        public static Error Map(this Error error, Func<Failure, Failure> map) 
            => error.Resolve(failure => map(failure), Error.From);

        public static Error Map(this Error error, Func<Failure, Exception> map) 
            => error.Resolve(failure => map(failure), Error.From);

        public static Error MapException(this Error error, Func<Exception, Exception> map) 
            => error.Resolve(Error.From, exception => map(exception));

        public static Error MapException(this Error error, Func<Exception, Failure> map) 
            => error.Resolve(Error.From, exception => map(exception));
    }
}