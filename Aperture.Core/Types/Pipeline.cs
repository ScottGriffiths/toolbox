﻿namespace Aperture.Core.Types
{
    using System;
    using Contracts;

    public static class Pipeline
    {
        public static Pipeline<T> Start<T>(T input) => input;
        public static Pipeline<T> Continue<T>(T input) => input;
        public static Pipeline<T> Cancel<T>(Error error) => error;
    }

    public struct Pipeline<TInput>
    {
        private bool Cancelled { get; }
        private TInput Input { get; }
        private Error Error { get; }

        private Pipeline(TInput input)
        {
            Cancelled = false;
            Input = input;
            Error = default(Error);
        }

        private Pipeline(Error error)
        {
            Cancelled = true;
            Input = default(TInput);
            Error = error;
        }

        public TResponse Resolve<TResponse>(Func<TInput, TResponse> success, Func<Error, TResponse> error)
        {
            try
            {
                return Cancelled ? error(Error) : success(Input);
            }
            catch (Exception e)
            {
                return error(e);
            }
        }

        public static implicit operator Pipeline<TInput>(TInput input) => new Pipeline<TInput>(input);
        public static implicit operator Pipeline<TInput>(Error error) => new Pipeline<TInput>(error);
        public static implicit operator Pipeline<TInput>(Failure failure) => new Pipeline<TInput>(failure);
        public static implicit operator Pipeline<TInput>(Exception exception) => new Pipeline<TInput>(exception);
    }


    public static class PipelineExtensions
    {
        public static TOutput Resolve<TOutput>(this Pipeline<TOutput> pipeline, IPipelineErrorHandler<TOutput> resolver)
            => pipeline.Resolve(output => output, resolver.Error);

        public static TOutput Resolve<TInput, TOutput>(this Pipeline<TInput> pipeline, IPipelineResolver<TInput, TOutput> resolver)
            => pipeline.Resolve(resolver.Success, resolver.Error);

        public static Pipeline<TResponse> Map<TRequest, TResponse>(this Pipeline<TRequest> pipeline, Func<TRequest, TResponse> map)
            => pipeline.Resolve(request => map(request), Pipeline.Cancel<TResponse>);

        public static Pipeline<TResponse> Then<TRequest, TResponse>(this Pipeline<TRequest> pipeline, Func<TRequest, Pipeline<TResponse>> then)
            => pipeline.Resolve(then, Pipeline.Cancel<TResponse>);
    }
}