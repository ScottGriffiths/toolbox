namespace Aperture.Core.Types
{
    public partial struct Failure
    {
        public int Code { get; }
        public string Message { get; }

        private Failure(int code, string message)
        {
            Code = code;
            Message = message;
        }

        private Failure(string message) : this(0, message) { }
        private Failure(int code) : this(code, string.Empty) { }

        public static Failure Because(string message) => new Failure(message);
        public static Failure Because(int code) => new Failure(code);
        public static Failure Because(int code, string message) => new Failure(code, message);

        public override string ToString() => 
            Code == 0 ? $"Message: '{Message}'" :
            Message == string.Empty ? $"Code: '{Code}'" :
            $"Code: '{Code}' Message: '{Message}'";
    }
}