namespace Aperture.Core.Contracts
{
    using Types;

    public interface IPipelineErrorHandler<out TOutput>
    {
        TOutput Error(Error error);
    }
}