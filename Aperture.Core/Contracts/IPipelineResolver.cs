namespace Aperture.Core.Contracts
{
    public interface IPipelineResolver<in TInput, out TOutput> : IPipelineErrorHandler<TOutput>
    {
        TOutput Success(TInput input);
    }
}